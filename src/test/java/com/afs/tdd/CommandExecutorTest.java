package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandExecutorTest {
    @Test
    public void should_return_01N_when_move_given_00N() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.N);

        //when
        Position actual = commandExecutor.move(position);

        //then
        assertEquals(new Position(0, 1, Direction.N), actual);
    }

    @Test
    public void should_return_0neg1S_when_move_given_00S() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.S);

        //when
        Position actual = commandExecutor.move(position);

        //then
        assertEquals(new Position(0, -1, Direction.S), actual);
    }

    @Test
    public void should_return_10E_when_move_given_00E() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.E);

        //when
        Position actual = commandExecutor.move(position);

        //then
        assertEquals(new Position(1, 0, Direction.E), actual);
    }

    @Test
    public void should_return_neg10W_when_move_given_00W() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.W);

        //when
        Position actual = commandExecutor.move(position);

        //then
        assertEquals(new Position(-1, 0, Direction.W), actual);
    }

    @Test
    public void should_return_00W_when_turn_left_given_00N() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.N);

        //when
        Position actual = commandExecutor.turnLeft(position);

        //then
        assertEquals(new Position(0, 0, Direction.W), actual);
    }

    @Test
    public void should_return_00E_when_turn_left_given_00S() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.S);

        //when
        Position actual = commandExecutor.turnLeft(position);

        //then
        assertEquals(new Position(0, 0, Direction.E), actual);
    }

    @Test
    public void should_return_00N_when_turn_left_given_00E() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.E);

        //when
        Position actual = commandExecutor.turnLeft(position);

        //then
        assertEquals(new Position(0, 0, Direction.N), actual);
    }

    @Test
    public void should_return_00S_when_turn_left_given_00W() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.W);

        //when
        Position actual = commandExecutor.turnLeft(position);

        //then
        assertEquals(new Position(0, 0, Direction.S), actual);
    }

    @Test
    public void should_return_00E_when_turn_right_given_00N() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.N);

        //when
        Position actual = commandExecutor.turnRight(position);

        //then
        assertEquals(new Position(0, 0, Direction.E), actual);
    }

    @Test
    public void should_return_00W_when_turn_right_given_00S() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.S);

        //when
        Position actual = commandExecutor.turnRight(position);

        //then
        assertEquals(new Position(0, 0, Direction.W), actual);
    }

    @Test
    public void should_return_00S_when_turn_right_given_00E() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.E);

        //when
        Position actual = commandExecutor.turnRight(position);

        //then
        assertEquals(new Position(0, 0, Direction.S), actual);
    }

    @Test
    public void should_return_00N_when_turn_right_given_00W() {
        //given
        CommandExecutor commandExecutor = new CommandExecutor();
        Position position = new Position(0, 0, Direction.W);

        //when
        Position actual = commandExecutor.turnRight(position);

        //then
        assertEquals(new Position(0, 0, Direction.N), actual);
    }
}