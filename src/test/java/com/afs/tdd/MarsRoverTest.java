package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarsRoverTest {

    @Test
    public void should_return_01E_when_executeCommand_MR_given_00N_as_current_position() {
        //given
        MarsRover marsRover = new MarsRover();

        //when
        Position actual = marsRover.executeCommand("M,R");

        //then
        assertEquals(new Position(0, 1, Direction.E), actual);
    }
}
