package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ControlCentreTest {

    @Test
    public void should_return_1_0_S_when_sendCommand_given_MR_then_MRM() {
        //given
        ControlCentre controlCentre = new ControlCentre();

        //when
        controlCentre.sendCommand("M,R");
        String actual = controlCentre.sendCommand("M,R,M");

        //then
        assertEquals("1 0 S", actual);
    }
}
