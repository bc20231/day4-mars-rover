package com.afs.tdd;

import java.util.Arrays;

public class MarsRover {
    private Position position = new Position(0, 0, Direction.N);
    private CommandExecutor commandExecutor = new CommandExecutor();

    public Position executeCommand(String command) {
        String[] commandList = command.split(",");
        Arrays.stream(commandList)
                .forEach(singleCommand -> {
                    switch (singleCommand) {
                        case "M":
                            this.position = this.commandExecutor.move(this.position);
                            break;
                        case "L":
                            this.position = this.commandExecutor.turnLeft(this.position);
                            break;
                        case "R":
                            this.position = this.commandExecutor.turnRight(this.position);
                            break;
                    }
        });
        return this.position;
    }

    public String reportPosition(Position newPosition) {
        return String.format("%d %d %s", newPosition.getX(), newPosition.getY(), newPosition.getDirection());
    }
}
