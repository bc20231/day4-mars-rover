package com.afs.tdd;

public class ControlCentre {
    private MarsRover marsRover = new MarsRover();

    public String sendCommand(String command) {
        Position newPosition = this.marsRover.executeCommand(command);
        return this.marsRover.reportPosition(newPosition);
    }
}
