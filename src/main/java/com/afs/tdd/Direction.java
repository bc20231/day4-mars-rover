package com.afs.tdd;

public enum Direction {
    N,
    S,
    E,
    W
}
