package com.afs.tdd;

public class CommandExecutor {
    public Position move(Position current) {
        int x = current.getX();
        int y = current.getY();

        switch (current.getDirection()) {
            case N:
                y = current.getY() + 1;
                break;
            case S:
                y = current.getY() - 1;
                break;
            case E:
                x = current.getX() + 1;
                break;
            case W:
                x = current.getX() - 1;
                break;
        }
        return new Position(x, y, current.getDirection());
    }

    public Position turnLeft(Position current) {
        switch (current.getDirection()) {
            case N:
                return new Position(current.getX(), current.getY(), Direction.W);
            case S:
                return new Position(current.getX(), current.getY(), Direction.E);
            case E:
                return new Position(current.getX(), current.getY(), Direction.N);
            case W:
                return new Position(current.getX(), current.getY(), Direction.S);
        }
        return current;
    }

    public Position turnRight(Position current) {
        switch (current.getDirection()) {
            case N:
                return new Position(current.getX(), current.getY(), Direction.E);
            case S:
                return new Position(current.getX(), current.getY(), Direction.W);
            case E:
                return new Position(current.getX(), current.getY(), Direction.S);
            case W:
                return new Position(current.getX(), current.getY(), Direction.N);
        }
        return current;
    }
}
